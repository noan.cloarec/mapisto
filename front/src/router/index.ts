import { createRouter, createWebHistory } from 'vue-router'
import StoryView from '../views/StoryView.vue'
import StoryCreationAssistantView from '@/views/StoryCreationAssistantView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/story/les-causes-de-la-guerre-de-cent-ans',
    },
    {
      path: '/story/:storyId',
      name: 'story',
      component: StoryView,
    },
    {
      path: '/create-story',
      name: 'create-story',
      component: StoryCreationAssistantView,
    },
  ],
})

export default router

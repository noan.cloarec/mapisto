import type { LngLatBounds } from 'maplibre-gl'

/**
 * An event to depict in a story
 */
export interface StoryEvent {
  /** The event's name, shown in the timeline */
  name: string
  /** The event's description, shown when the event is focused */
  description: string
  /** The event's location, to show it on the map */
  location: Coordinates
  /** The event's time */
  time: Date
  /** An optional image to depict the event */
  imageUrl?: string
  /** The bounding box of the map to show when the story focuses on the event */
  sceneBounds?: LngLatBounds
}

/** A geographic point */
interface Coordinates {
  latitude: number
  longitude: number
}

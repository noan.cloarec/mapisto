import charles4 from '@/assets/charles_iv_france.png'
import edward2 from '@/assets/edward_ii_england.png'
import farmers from '@/assets/farmers.jpg'
import flanders from '@/assets/flanders.png'
import jeanne_ii_navarre from '@/assets/jeanne_ii_navarre.png'
import louis_x from '@/assets/louis_x.png'
import { LngLat, LngLatBounds } from 'maplibre-gl'
import { sumYearSpan, type Discontinuity } from './Discontinuity'
import type { StoryEvent } from './StoryEvent'

/** A story to be told in mapisto */
export interface Story {
  name: string
  /** The events to be told */
  steps: StoryEvent[]
  /** Additional parameters for the timeline that is displayed when telling the story */
  timelineOptions?: {
    discontinuities: Discontinuity[]
  }
}

/**
 * Returns the steps of a story ordered by their apparition date, instead of the normal narration order
 * @param story The story whose sorted steps must be computed
 * @returns Its steps, but sorted by apparition date
 */
export const sortedSteps = (story: Story) =>
  [...story.steps].sort((a, b) => a.time.getTime() - b.time.getTime())

/**
 * A story spans accross a time range (for example a story starting in 1500 and ending in 2000 spans through 500 years)
 * This function computes a story time span in years.
 * As a story can be discontinued (see discontinuities field in timelineOptions), the year span of the discontinuities is substracted from the result.
 * @param story
 * @returns
 */
export const getYearSpanWithoutDiscontinuities = (story: Story) => {
  const steps = sortedSteps(story)
  const yearSpan = steps.slice(-1)[0].time.getUTCFullYear() - steps[0].time.getUTCFullYear()
  const discontinuities = story.timelineOptions?.discontinuities
    ? story.timelineOptions!.discontinuities!
    : []
  return yearSpan - sumYearSpan(discontinuities)
}

export const hundredYearsWar: Story = {
  name: 'Les causes de la guerre de cent ans',
  steps: [
    {
      name: 'Le royaume de France au début du 14ème siècle',
      description: `Au début du XIVe siècle, la France compte environ ?? millions d'habitants, sa population s'est considérablement accru depuis le Xe siècle.`,
      time: new Date('1300-01-01'),
      location: {
        latitude: 45.98,
        longitude: 0.35,
      },
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
    {
      name: 'La Flandre surpeuplée',
      description: `Certaines régions deviennent surpeuplée, comme les Flandres qui cherchent déjà à gagner du terrain sur la mer.`,
      time: new Date('1300-01-01'),
      location: {
        latitude: 51.06,
        longitude: 3.72,
      },
      imageUrl: flanders,
    },
    {
      name: 'Le refroidissement',
      description: `Le climat se refroidit et les mauvaises récoltes s'accumulent, 1314, 1315 et 1316 sont 3 années de famine successives`,
      time: new Date('1314-01-01'),
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      imageUrl: farmers,
    },
    {
      name: 'Louis X règne sur la France',
      description: `Depuis 1305, Louis X règne sur la France`,
      time: new Date('1305-10-07'),
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      imageUrl: louis_x,
    },
    {
      name: "Edouard II règne sur l'Angleterre",
      description: `L'Angleterre quant à elle est gouvernée par Edouard II depuis 1308`,
      time: new Date('1308-02-25'),
      location: {
        latitude: 51.5,
        longitude: -0.133333,
      },
      imageUrl: edward2,
      sceneBounds: new LngLatBounds(
        new LngLat(-11.174654, 56.636902),
        new LngLat(3.897028, 48.551529),
      ),
    },
    {
      name: "La Guyenne revient à la couronne d'Angleterre",
      description: `Mais il est également duc de Guyenne, le duché étant revenu à la couronne d'Angleterre en 1154 à la suite du mariage d'Aliénor d'Aquitaine avec Henri II Plantagenêt, futur roi d'Angleterre.`,
      time: new Date('1154-12-19'),
      location: {
        latitude: 45.19,
        longitude: 0.923,
      },
      sceneBounds: new LngLatBounds(
        new LngLat(-11.174654, 56.636902),
        new LngLat(9.877492, 41.876112),
      ),
    },
    {
      name: 'Crise de succession de 1316',
      description: `Louis X meurt en 1316. Son unique héritier mâle meurt prématurément.
        Jeanne de Navarre, fille de Louis X est son unique héritière. C'est la première fois qu'un roi Capétien meurt sans laisser d'héritier mâle.
        Doit-elle accéder au trône ? Craignant de la voir marier un souverain étranger qui dirigerait le pays, les états généraux décident que "femme ne succède pas au royaume de France".
        Le trône revient à Philippe V, frère du roi et régent.`,
      time: new Date('1316-06-05'),
      location: {
        latitude: 45.19,
        longitude: 0.923,
      },
      imageUrl: jeanne_ii_navarre,
    },
    {
      name: 'Mort de Charles IV',
      description: `Philippe V n'est pas plus fertile que Louis X et meurt quelques années plus tard en laissant le trône à son autre frère Charles IV, dernier des fils de Philippe IV Le Bel.
        En 1328, Charles IV meurt sans laisser d'héritier.
        Isabelle, soeur de Charles IV et elle aussi fille du roi Philippe Le Bel a épousé Edouard II  d'Angleterre. Elle a enfanté Edouard III.
        Edouard III d'Angleterre est donc le dernier descendant mâle de Philippe Le Bel par sa mère.`,
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      time: new Date('1328-02-01'),
      imageUrl: charles4,
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
    {
      name: 'Avènement de Philippe VI de Valois',
      description: `Les pairs et barons de France réunis empêchent Edouard III de succéder au trône de France.
        Ils affirment qu'Isabelle ne peut pas transmettre un droit à la succession puisque, comme Jeanne de Navarre, elle ne peut pas prétendre au trône.
        Les propagandistes français de la guerre de Cent ans justifieront plus tard cette décision en prétendant que cette loi était appliqué par les Francs Saliens au temps de Clovis.
        Elle restera connue sous le nom de loi salique et écartera pour toujours les femmes de la couronne de France.
        Philippe de Valois, cousin du défunt roi, devient roi de France.`,
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      time: new Date('1328-04-01'),
      imageUrl: charles4,
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
    {
      name: `L'incident de Saint-Sardos et la confiscation de la Guyenne`,
      description: `Les rois de France de plus en plus influents s'impliquent dans les querelles autour de la frontière de l'Aquitaine.
        Les seigneurs Aquitains inquiets de voir le roi s'immiscer chez eux ripostent, prennent la bastide de Saint-Sardos et exécutent la garnison.
        Le Parlement de Paris confisque le duché d'Aquitaine et Edouard II est contraint d'envoyer son fils Edouard II prêter hommage et affirmer sa vassalité au roi de France.
        `,
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      time: new Date('1323-10-16'),
      imageUrl: charles4,
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
    {
      name: `L'hommage d'Edouard III à Philippe VI en 1329`,
      description: `C'est ce qu'Edouard III fait en juin 1329. Philippe VI ne restitue pas l'Agenais à Edouard, qui ne considère pas pour autant renoncer à cette terre`,
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      time: new Date('1323-10-16'),
      imageUrl: edward2,
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
    {
      name: `L'Auld Alliance`,
      description: `Depuis 1295, les Français et les Ecossais entretiennent une alliance, prévoyant que si une des 2 parties subissait une attaque de l'Angleterre, l'autre l'envahirait.
        Cette Vieille Alliance (ou Auld Alliance en scot) sera prolongée jusqu'au XVIe siècle.`,
      location: {
        latitude: 48.847778,
        longitude: 2.439167,
      },
      time: new Date('1295-10-23'),
      imageUrl: charles4,
      sceneBounds: new LngLatBounds(
        new LngLat(-10.16446545556434, 42.84253298018021),
        new LngLat(14.393272875413885, 51.22972809832089),
      ),
    },
  ],
  timelineOptions: {
    discontinuities: [
      {
        from: new Date('1155-02-01'),
        to: new Date('1294-02-01'),
      },
    ],
  },
}

# Mapisto

## Installation instruction

```sh
# Install git lfs
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt install git-lfs

git clone https://gitlab.com/noan-cloarec/mapisto.git
cd mapisto
git lfs pull # Download map tiles
docker compose up -d
```

### Optional : enable https

- [Install certbot](https://certbot.eff.org/instructions?ws=other&os=pip)
- Generate the certificate with `sudo certbot certonly --standalone`
- Run `docker compose -f docker-compose.yml -f docker-compose-add-https.yml up -d`

## Development

```sh
cd mapisto
pre-commit install
# In first tab
cd front/
npm run dev
# In second tab
docker run  -v ./tileserver:/data -p 8080:8080 maptiler/tileserver-gl --file /data/planet-water-11.mbtiles -c /data/tileserver_config.json
```

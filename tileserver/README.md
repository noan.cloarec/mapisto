## Usage

docker run --rm -it -v $(pwd):/data -p 8080:8080 maptiler/tileserver-gl --file /data/planet-water-11.mbtiles -c /data/tileserver_config.json

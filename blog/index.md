---
layout: home
title: Mapisto's blog
---

<article markdown="1">
![img](https://images.ctfassets.net/xz1dnu24egyd/1hnQd13UBU7n5V0RsJcbP3/769692e40a6d528e334b84f079c1f577/gitlab-logo-100.png)

[How to build a blog with GitLab Pages](https://dev.to/noan/heading-missing-2jbc-temp-slug-9627667?preview=97fcb6002b2ae2cdb5fd14e3366d3deae0e9ea846d05794a026786f2fecb3c5a32ae5ab533a1b7816dd0b6618deb5d9eef17d9d2acf9722761ae682c){:target="\_blank"}

A beginner's guide to create a blog hosted on GitLab Pages with continuous deployment

</article>
